//
//  TableViewController.swift
//  Core Data
//
//  Created by Javier Yllescas on 1/6/20.
//  Copyright © 2020 Javier Yllescas. All rights reserved.
//

import UIKit
import CoreData

class TableViewController: UITableViewController {

    var manageObjects: [NSManagedObject] = []
    var List:[String] = ["Puebla", "México", "Mérida"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //Llamado a appDelegate
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let managedContext = appDelegate!.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Lista")
        
        do{
            
            manageObjects = try managedContext.fetch(fetchRequest)
        }catch let error as NSError{
            print("\(error.userInfo)")
        }
        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return manageObjects.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)

        //cell.textLabel?.text = List[indexPath.row]
        
        let objCore=manageObjects[indexPath.row]
        cell.textLabel!.text = objCore.value(forKey: "palabra") as? String
        //cell.textLabel!.text = objCore.value(forKeyPath: "edad") as? String

        return cell
    }
    
    @IBAction func Add(_ sender: Any) {
        let alert = UIAlertController(title: "Nueva Persona", message: "Agregar nueva persona", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Agregar", style: .default, handler: {
            (action:UIAlertAction) -> Void in
            let textField = alert.textFields!.first
            self.guardarPalabra(palabra: textField!.text!)
            self.tableView.reloadData)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
        alert.addTextField(configurationHandler: nil)
        present(alert, animated: true, completion: nil)
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let objCore = manageObjects[indexPath.row]
            let appDelegate = UIApplication.shared.delegate as? AppDelegate
            let managedContext = appDelegate!.persistentContainer.viewContext
            managedContext.delete(objCore)
            manageObjects.remove(at: indexPath.row)
            
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }


    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func guardarPalabra(palabra: String){
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let managedContext = appDelegate!.persistentContainer.viewContext
        
        let entity = NSEntityDescription.entity(forEntityName: "Lista", in: managedContext)!
        let managedObject = NSManagedObject(entity: entity, insertInto: managedContext)
        
        managedObject.setValue(palabra,forKeyPath: "palabra")
        
        do{
            try managedContext.save()
            manageObjects.append(managedObject)
        }catch let error as NSError{
            
            print("\(error.userInfo)")
        }
        
    }

    
    func guardarEdad(edad: Int){
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let managedContext = appDelegate!.persistentContainer.viewContext
        
        let entity = NSEntityDescription.entity(forEntityName: "Lista", in: managedContext)!
        let managedObject = NSManagedObject(entity: entity, insertInto: managedContext)
        
        managedObject.setValue(edad,forKeyPath: "edad")
        
        do{
            try managedContext.save()
            manageObjects.append(managedObject)
        }catch let error as NSError{
            
            print("\(error.userInfo)")
        }
        
    }
}
